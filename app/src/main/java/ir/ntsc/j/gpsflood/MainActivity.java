package ir.ntsc.j.gpsflood;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ToggleButton;

import java.net.InetAddress;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakelock;
    ToggleButton tbGPS;
    EditText hostAddrEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mPowerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakelock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, this.getClass().getName());
        boolean gpsFlg = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        Log.d("GPS Enabled", gpsFlg ? "OK" : "NG");

        tbGPS = (ToggleButton) findViewById(R.id.toggleGPSButton);
        tbGPS.setOnCheckedChangeListener(this);
        hostAddrEditText = (EditText) findViewById(R.id.hostAddrEditText);

        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("GPS", "Location Changed");
                Log.d("GPS", location.getLatitude() + "," + location.getLongitude());

                NetworkMessage nm = new NetworkMessage();
                String msg = location.getLatitude() + "," + location.getLongitude() + "\n";
                nm.data = msg.getBytes();
                nm.port = Config.port;
                try {
                    nm.addr = InetAddress.getByName(hostAddrEditText.getText().toString());
                } catch(Exception e) {
                    Log.e("GPS", "Exception", e);
                }
                AsyncNetwork asyncNetwork = new AsyncNetwork();
                asyncNetwork.execute(nm);
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d("GPS", "GPS Provider Disabled");
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d("GPS", "GPS Provider Enabled");
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d("GPS", "GPS Status Changed");
            }
        };
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(getPackageManager().checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName())
                == PackageManager.PERMISSION_GRANTED) {
            if(isChecked) { // Turn on
                Log.d("GPS", "GPS ON");
                mWakelock.acquire();
                Location l = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(l == null)
                    Log.d("GPS", "Last: null");
                else
                    Log.d("GPS", "Last: " + l.getLatitude() + "," + l.getLongitude());

                mLocationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        500, // 通知のための最小時間間隔（ミリ秒）
                        0, // 通知のための最小距離間隔（メートル）
                        mLocationListener
                );
            } else { // Turn off
                Log.d("GPS", "GPS OFF");
                mLocationManager.removeUpdates(mLocationListener);
                mWakelock.release();
            }
        } else {
            Log.d("GPS", "No Permission");
        }
    }
}
