package ir.ntsc.j.gpsflood;

import java.net.InetAddress;

public class NetworkMessage {
    InetAddress addr;
    int port;
    byte[] data;
}
