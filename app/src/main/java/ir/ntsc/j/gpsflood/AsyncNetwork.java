package ir.ntsc.j.gpsflood;

import android.os.AsyncTask;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class AsyncNetwork extends AsyncTask<NetworkMessage, Integer, Integer> {

    @Override
    protected Integer doInBackground(NetworkMessage... nm) { // Send UDP Packet
        try {
            Log.d("GPS", "Sending packet to " + nm[0].addr.toString() + ", Port " + nm[0].port);
            Log.d("GPS", "Content:\n" + nm[0].data.toString());
            DatagramPacket dp = new DatagramPacket(nm[0].data, nm[0].data.length, nm[0].addr, nm[0].port);
            DatagramSocket ds = new DatagramSocket();
            ds.send(dp);
            Log.d("GPS", "UDP Packet Sent");
        } catch(Exception e) {
            Log.d("GPS", "Network error");
            Log.e("GPS", "Exception: ", e);
        }
        return 0;
    }
}
